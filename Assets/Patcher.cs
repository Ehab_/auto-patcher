﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class Patcher : MonoBehaviour {

	public Text buildDisplay;
	public GameObject downloadPrompt;
	public Button downloadBtn;
	public GameObject installPrompt;
	public Button installBtn;
	private string buildNumber;
	// Use this for initialization
	void Start () {
		
		buildNumber = Application.version.ToString ();
		string path = Application.persistentDataPath + "/game_version.txt";
		Debug.Log (path);
		if (!File.Exists (path)) {
			File.WriteAllText (path, Application.version);
		} else {
			GameObject.Find ("Log").GetComponent<Text> ().text = "File exists!";
			StreamReader reader = new StreamReader(path);
			string readBuildNumber = reader.ReadLine();
			GameObject.Find ("Log").GetComponent<Text> ().text = readBuildNumber;
			if (readBuildNumber != buildNumber) {
				// Patcher here
				GameObject.Find ("Log").GetComponent<Text> ().text = "Reading new update!";
				downloadPrompt.SetActive (true);
				//downLoadFromServer();
			}
		}
		buildDisplay.text = "Build Number: " + buildNumber;
	}
	
	void Update()
	{
		buildNumber = Application.version;
		buildDisplay.text = "Build Number: " + buildNumber;
	}

	public void DownloadCofirmation ()
	{
		downLoadFromServer ();
	}
	public void InstallConfirmation()
	{
		downloadPrompt.SetActive (false);
		string savePath = Path.Combine(Application.persistentDataPath, "update.apk");
		//Install APK
		installApp(savePath);
	}
	public void downLoadFromServer()
	{
		/*string url = "apk path";


		string savePath = Path.Combine(Application.persistentDataPath, "data");
		savePath = Path.Combine(savePath, "AntiOvr.apk");

		Dictionary<string, string> header = new Dictionary<string, string>();
		string userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
		header.Add("User-Agent", userAgent);
		WWW www = new WWW(url, null, header);

		//Wait untill download is done
		while (!www.isDone)
		{
			GameObject.Find("Log").GetComponent<Text>().text = "Stat: " + www.progress;
			yield return null;
		}

		byte[] yourBytes = www.bytes;

		GameObject.Find("Log").GetComponent<Text>().text = "Done downloading. Size: " + yourBytes.Length;

		//Create Directory if it does not exist, it should exist in this case..but for collective this check is important in case we retrieve version from web
		if (!Directory.Exists(Path.GetDirectoryName(savePath)))
		{
			Directory.CreateDirectory(Path.GetDirectoryName(savePath));
			GameObject.Find("Log").GetComponent<Text>().text = "Created Dir";
		}

		try
		{
			//Saving the file
			System.IO.File.WriteAllBytes(savePath, yourBytes);
			GameObject.Find("Log").GetComponent<Text>().text = "Saved Data";
		}
		catch (Exception e)
		{
			Debug.LogWarning("Failed To Save Data to: " + savePath.Replace("/", "\\"));
			Debug.LogWarning("Error: " + e.Message);
			GameObject.Find("Log").GetComponent<Text>().text = "Error Saving Data";
		}*/

		//string savePath = Path.Combine(Application.persistentDataPath, "update.apk");
		//Install APK
		downloadPrompt.SetActive (false);
		installPrompt.SetActive (true);
	}

	void installApp(string apkPath)
	{
		GameObject.Find("Log").GetComponent<Text>().text = "Installing App";

		try
		{
			//Get Activity then Context
			AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject unityContext = currentActivity.Call<AndroidJavaObject>("getApplicationContext");

			//Get the package Name
			string packageName = unityContext.Call<string>("getPackageName");
			string authority = packageName + ".fileprovider";

			AndroidJavaClass intentObj = new AndroidJavaClass("android.content.Intent");
			string ACTION_VIEW = intentObj.GetStatic<string>("ACTION_VIEW");
			AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent", ACTION_VIEW);


			int FLAG_ACTIVITY_NEW_TASK = intentObj.GetStatic<int>("FLAG_ACTIVITY_NEW_TASK");
			int FLAG_GRANT_READ_URI_PERMISSION = intentObj.GetStatic<int>("FLAG_GRANT_READ_URI_PERMISSION");

			//File fileObj = new File(String pathname);
			AndroidJavaObject fileObj = new AndroidJavaObject("java.io.File", apkPath);
			//FileProvider object that will be used to call it static function
			AndroidJavaClass fileProvider = new AndroidJavaClass("android.support.v4.content.FileProvider");
			//getUriForFile(Context context, String authority, File file)
			AndroidJavaObject uri = fileProvider.CallStatic<AndroidJavaObject>("getUriForFile", unityContext, authority, fileObj);

			intent.Call<AndroidJavaObject>("setDataAndType", uri, "application/vnd.android.package-archive");
			intent.Call<AndroidJavaObject>("addFlags", FLAG_ACTIVITY_NEW_TASK);
			intent.Call<AndroidJavaObject>("addFlags", FLAG_GRANT_READ_URI_PERMISSION);
			StartCoroutine(CloseApp());
			currentActivity.Call("startActivity", intent);
			GameObject.Find("Log").GetComponent<Text>().text = "Success";
		}
		catch (System.Exception e)
		{
			GameObject.Find("Log").GetComponent<Text>().text = "Error is: " + e.Message;
		}
	}
	IEnumerator CloseApp()
	{
		yield return new WaitForSeconds (0.1f);
		Application.Quit ();
	}
}
